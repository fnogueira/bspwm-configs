## Instalando pacotes

```
sudo install pacman -S xf86-video-qxl xorg xorg-xinit bspwm sxhkd dmenu nitrogen picom xfce4-terminal chromium arandr --needed
```
ou

```
sudo pacman -S xorg bspwm sxhkd dmenu terminator lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings nitrogen lxappearance picom nautilus --needed
```


## Criando diretórios

```
mkdir -p ~/.config/{bspwm,sxhkd}
```

## Copiando arquivos

```
cp /usr/share/doc/bspwm/examples/bspwmrc .config/bspwm/
cp /usr/share/doc/bspwm/examples/sxhkdrc .config/sxhkd/
```
ou

```
install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc .config/bspwm/
install -Dm644 /usr/share/doc/bspwm/examples/sxhkdrc .config/sxhkd/
```
## Editando atalhos (sxhkdrc)

```
vim .config/sxhkd/sxhkdrc
```




